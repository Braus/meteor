import {Meteor} from 'meteor/meteor';
import {Players} from '../imports/api/players';

Meteor.startup(() => {
    if(Meteor.isServer){
        Meteor.publish('players', function() {
            return Players.find({});
        })
    }
})
