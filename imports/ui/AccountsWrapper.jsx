import {Template} from 'meteor/templating'
import {Blaze} from 'meteor/blaze'
import React, {Component} from 'react'
import ReactDOM from 'react-dom'


export default class AccountsWrapper extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.view = Blaze.render(Template.loginButtons,
                    ReactDOM.findDOMNode(this.refs.containerlogin))

    }
    componentWillUnmount() {
        Blaze.remove(this.view)
    }

    render() {
        return <span ref="containerlogin" />
    }
}
